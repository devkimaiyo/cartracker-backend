package com.web.tracker.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "spring.rabbitmq")
public class RabbitmqProperties {

    private String virtualHost;

    private String host;

    private String username;

    private int port;

    private String password;

    private Integer minConsumer;

    private Integer maxConsumer;

    private Integer prefetchCount;
}
