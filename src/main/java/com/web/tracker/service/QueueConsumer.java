package com.web.tracker.service;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.web.tracker.model.FunData;
import com.web.tracker.model.M2SData;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class QueueConsumer {
    private final SimpMessagingTemplate template;
    private static final Logger log = LoggerFactory.getLogger(QueueConsumer.class);
    private static ObjectMapper mapper = new ObjectMapper();

    Gson gson = new Gson();

    public QueueConsumer(SimpMessagingTemplate template) {
        this.template = template;
    }

    @RabbitHandler
    @RabbitListener(queues = "${spring.rabbitmq.dataQueue}", containerFactory = "rabbitListenerContainerFactory")
    public void greeting(@Payload M2SData data) throws Exception {
        try {
            List<FunData> funs = data.getData();
            for (FunData funData : funs) {
                if ("00".equals(funData.getFunCode())) {
                    this.template.convertAndSend("/topic/equipment", mapper.writeValueAsString(funData.getFunData()));
   
                } else if ("01".equals(funData.getFunCode())) {
                    this.template.convertAndSend("/topic/seat", mapper.writeValueAsString(funData.getFunData()));
                } else if ("0A".equals(funData.getFunCode())) {
                    this.template.convertAndSend("/topic/gps", mapper.writeValueAsString(funData.getFunData()));
                   System.out.println(funData.getFunData());
                } else {
                    this.template.convertAndSend("/topic/unknown", mapper.writeValueAsString(funData.getFunData()));

                }
            }
        } catch (Exception e) {
            log.error("process error", e);
        }

    }

    @MessageExceptionHandler
    @SendTo("/queue/errors")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }
}
