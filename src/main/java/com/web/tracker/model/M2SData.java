package com.web.tracker.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class M2SData {
    private String devId;
    private Integer frameId;
    private String pipe;
    private List<FunData> data;
}
