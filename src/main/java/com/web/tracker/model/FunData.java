package com.web.tracker.model;

import lombok.Data;

import java.util.Map;

@Data
public class FunData {

    // code
    private String funCode;

    // data
    private Map<String, Object> funData;

    public String getFunCode() {
        return funCode;
    }

    public void setFunCode(String funCode) {
        this.funCode = funCode;
    }

    public Map<String, Object> getFunData() {
        return funData;
    }

    public void setFunData(Map<String, Object> funData) {
        this.funData = funData;
    }
}
