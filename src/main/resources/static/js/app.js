var stompClient = null;

function setConnected(connected) {
	$("#connect").prop("disabled", connected);
	$("#disconnect").prop("disabled", !connected);
	if (connected) {
		$("#conversation").show();
	}
	else {
		$("#conversation").hide();
	}
	$("#greetings").html("");
}

function connect() {
	const socket = new SockJS('/gs-guide-websocket');
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function (frame) {
		setConnected(true);
		console.log('Connected: ' + frame);
		stompClient.subscribe('/topic/greetings', function (greeting) {
			showGreeting(JSON.parse(greeting.body).content);
		});
	});
}

function disconnect() {
	if (stompClient !== null) {
		stompClient.disconnect();
	}
	setConnected(false);
	console.log("Disconnected");
}

function sendName() {
	stompClient.send("/app/hello", {}, JSON.stringify({'name': $("#name").val()}));
}

function showGreeting(message) {
	$("#greetings").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
	$("form").on('submit', function (e) {
		e.preventDefault();
	});
	$( "#connect" ).click(function() { connect(); });
	$( "#disconnect" ).click(function() { disconnect(); });
	$( "#send" ).click(function() { sendName(); });
});
$(function() {
	var token = "<%= user.token %>";
	var trackID = "<%= trackID %>";
	var markerStart = null;
	var markerFinish = null;
	var polyline = null;
	var track = null;
	var user = null;
	var WSClient = null;
	var bounds = null;
	function loadTrackFullInfo() {
		var url = "http://"+IP+":8080/api/track/"+trackID+"/full";
		var xhr = new XMLHttpRequest();
		xhr.open('GET', url, true);
		xhr.setRequestHeader('Authorization', 'Bearer ' + token);
		xhr.responseType = 'json';
		xhr.onload = function () {
			var status = xhr.status;
			if (status === 200) {
				//console.log(xhr.response);

				saveTrackFullInfo(xhr.response);
			} else {
				console.log("unauthotirize");
			}
		};

		xhr.send();
	}
	function saveTrackFullInfo(data) {
		user = data.user;
		track = data.track;
		track.latLongs = [];
		bounds = new google.maps.LatLngBounds();
		for(var i =0;i<data.locations.length; i++) {
			var latLong = new google.maps.LatLng(data.locations[i].latitude, data.locations[i].longitude);
			track.latLongs.push(latLong);
			bounds.extend(latLong);
			//console.log((26+i) + ": " +  data.locations[i].latitude + ", " + data.locations[i].longitude);

		}
		openTrack(track);

		$("#panelTitle").text(track.name);
		createBreadcrumbs();
	}
	function createMarker(latLong, color, labelText) {
		return new google.maps.Marker({
			map: map,
			position: latLong,
			icon: {
				url: "/images/"+color.name+".svg",
				scaledSize: new google.maps.Size(25, 41),
				labelOrigin: new google.maps.Point(13, -10)
			},
			labelClass: "labels",
			label: {
				text: labelText,
				fontSize: '12px'
			}
		});
	}
	function createPolyline(locations, colorHex) {
		var polyline = new google.maps.Polyline({
			map: map,
			path: locations,
			geodesic: true,
			strokeColor: '#' + colorHex,
			strokeOpacity: 1.0,
			strokeWeight: 4
		});
		return polyline;
	}
	$("#openUserTrack").click(function () {
		showUserTrack()
	});
	$("#userTrack .close-panel").click(function () {
		$("#userTrack").fadeOut();
		$("#openUserTrack").fadeIn();
	});
	function showUserTrack() {
		$("#openUserTrack").fadeOut(0);
		$("#userTrack").fadeIn();
	}
	showUserTrack();
	function openTrack(track) {
		showTrack(track);
		fillTrackInfo(track);
	}
	function showTrack(track) {
		markerStart = createMarker(track.latLongs[0], markerColors[0], "Początek")
		markerFinish = createMarker(track.latLongs[track.latLongs.length-1], markerColors[0], "Koniec")
		polyline = createPolyline(track.latLongs, markerColors[0].hex[0]);

		map.fitBounds(bounds);
	}
	function fillTrackInfo(track) {
		var startDate = new Date(track.startTimestamp);
		var finishDate = new Date(track.finishTimestamp);
		$("#startTime").text(formatDateTime(startDate));
		$("#finishTime").text(formatDateTime(finishDate));

		var dist = prepareDistance(track.distance, false);
		$("#distance").text(dist.value + " " + dist.unit);
		$("#openUserMap").attr("href", "/map/user/"+user._id);
		$("#openUserMap span").text(user.username);
		$("#openUserTracksMap").attr("href", "/map/user/"+user._id+"/tracks");
	}
	function formatDateTime(date) {
		return date.getHours()
			+ ":" + leadingZero(date.getMinutes())
			+ ", " + date.getDate()
			+ " " + getMonthShortName(date.getMonth())
			+ " " + date.getFullYear();
	}
	function createBreadcrumbs() {
		var elLiveMap = document.createElement("a");
		$(elLiveMap).attr("href", "/map");
		$(elLiveMap).text("Live Map");
		var elUserMap = document.createElement("a");
		$(elUserMap).attr("href", "/map/user/" + user._id);
		$(elUserMap).text(user.username);
		var elUserTracksMap = document.createElement("a");
		$(elUserTracksMap).attr("href", "/map/user/" + user._id + "/tracks");
		$(elUserTracksMap).text("Trasy");
		var elTrackMap = document.createElement("a");
		$(elTrackMap).attr("href", "/map/track/" + track._id);
		$(elTrackMap).text(track.name);

		var breadcrumbsWrap = $("#breadcrumbs .breadcrumbs-wrap");
		var separator = "&nbsp;&raquo;&nbsp";
		breadcrumbsWrap.append(elLiveMap);
		breadcrumbsWrap.append(separator);
		breadcrumbsWrap.append(elUserMap);
		breadcrumbsWrap.append(separator);
		breadcrumbsWrap.append(elUserTracksMap);
		breadcrumbsWrap.append(separator);
		breadcrumbsWrap.append(elTrackMap);
		$("#breadcrumbs").css("opacity", "1");
	}
	loadMap();
	loadTrackFullInfo();

});